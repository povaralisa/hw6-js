"use strict";

const array = ["666", 666, "Alisa", null, NaN, false, true, undefined];

function filterBy(array, type) {
  return array.filter((item) => typeof item !== type);
}
console.log(filterBy(array, "string"));

